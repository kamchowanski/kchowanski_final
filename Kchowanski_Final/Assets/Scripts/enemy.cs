﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemy : MonoBehaviour
{
    Transform target;
    NavMeshAgent navAgent;
    public GameObject FPSController;
    public float damageAmount = 5f;    
    [SerializeField]
    float attackTime = .9f;
    bool attack = true;
    Animator anim;
    
    
    void Start()
    {
        FPSController = GameObject.FindGameObjectWithTag("Player");
        navAgent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(transform.position, FPSController.transform.position);
            if (FPSController != null)
        {
            navAgent.SetDestination(FPSController.transform.position);
        }

        if (distance < 1.5 && attack && !PlayerHealth.singleton.isDead)
        {
            StartCoroutine(AttackTime());
        }
    }



    IEnumerator AttackTime()
    {
        anim.SetTrigger("punching");
        attack = false;
        yield return new WaitForSeconds(.1f);
        PlayerHealth.singleton.DamagePlayer(damageAmount);
        
        yield return new WaitForSeconds(attackTime);
        attack = true;
    }

}
