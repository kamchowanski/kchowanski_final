﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class repeatSpawner : MonoBehaviour
{
    public GameObject Enemy;
    public float spawnTime = 5f;

    void Start()
    {
        InvokeRepeating("SpawnEnemy", spawnTime, spawnTime);
    }


    void Update()
    {
    }
    void SpawnEnemy()
    {
        var newEnemy = GameObject.Instantiate(Enemy,transform.position,Enemy.transform.rotation);
    }
}
