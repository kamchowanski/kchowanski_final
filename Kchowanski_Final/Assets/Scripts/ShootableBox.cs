﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShootableBox : MonoBehaviour
{
    Animator anim;

    //The box's current health point total
    public int currentHealth = 1;
    public GameObject Health_powerup;
    public GameObject SpeedUp;

    void Start()
    {
        
    }

   

    public void Damage(int damageAmount)
    {
        //subtract damage amount when Damage function is called
        currentHealth -= damageAmount;
        

        //Check if health has fallen below zero
        if (currentHealth <= 0)
        {
            
            //EnemyDeathAnim();
            Destroy(gameObject);
            PlayerHealth.singleton.setScore(10);
            healthdrop();
            speedDrop();



        }
    }


    public void EnemyDeathAnim()
    {
        anim.SetTrigger("dead");
    }

    public void healthdrop()
    {
        if (Random.value > 0.90) 
        {
          Instantiate(Health_powerup, transform.position, Health_powerup.transform.rotation);
        }        
    }

    public void speedDrop()
    {
        if (Random.value > 0.90)
        {
            Instantiate(SpeedUp, transform.position, SpeedUp.transform.rotation);
        }
    }

}