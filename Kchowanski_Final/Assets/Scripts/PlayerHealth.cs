﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public static PlayerHealth singleton;
    public float currentHealth;
    public float maxHealth = 100f;
    public bool isDead = false;
    public Slider healthSlider;
    public Text healthCounter;
    public int score;
    public Text loseText;
    public AudioClip Hurtclip;
    public AudioSource hurtsource;
    public AudioClip healclip;
    public AudioSource healsource;


    private Text scoreText;

    private void Awake()
    {
        singleton = this;
        scoreText = GameObject.FindGameObjectWithTag("scoreText").GetComponent<Text>();
    }
    void Start()
    {        
        currentHealth = maxHealth;
        healthSlider.value = currentHealth;
        UpdateHealthCounter();
        setScore(0);
        loseText.text = "";
        hurtsource.clip = Hurtclip;
        healsource.clip = healclip;
    }

    // Update is called once per frame
    
    public void DamagePlayer(float damage)
    {
        if(currentHealth > 0)
        {
            currentHealth -= damage;
            hurtsource.Play();
            healthSlider.value = currentHealth;
            print("H"+currentHealth);
            print("D"+damage);

        }
        else
        {
            Dead();
        }
        UpdateHealthCounter();
    }

    void Dead()
    {
        currentHealth = 0;
        healthSlider.value = 0;
        UpdateHealthCounter();
        Debug.Log("player is dead");
        loseText.text = "You Died!";

        Invoke("RestartLevel", 2f);
    }

    void UpdateHealthCounter()
    {
        healthCounter.text = currentHealth.ToString();
    }

    public void setScore(int toadd)
    {
        score += toadd;

        scoreText.text = "Score: " + score.ToString();
    }

    void RestartLevel()
    {
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("health"))
        {
            healsource.Play();
            currentHealth += 10;
            healthSlider.value += 10;
            UpdateHealthCounter();


        }
    }
}
