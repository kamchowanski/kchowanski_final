﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class repeatbigspawner : MonoBehaviour
{
    public GameObject Enemy;
    public float spawnTime = 10f;
    public static int count;
    bool beginbigspawn = true;


    void Start()
    {
        
    }


    void Update()
    {
        if (PlayerHealth.singleton.score >= 200 && beginbigspawn)
        {
            beginbigspawn = false;
            InvokeRepeating("SpawnEnemy", spawnTime, spawnTime);

        }
    }
    void SpawnEnemy()
    {
        var newEnemy = GameObject.Instantiate(Enemy, transform.position, Enemy.transform.rotation);
    }
}
